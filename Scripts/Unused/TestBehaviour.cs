﻿namespace Codefarts.PropertyModifiers.Scripts
{
    using System.Runtime.Serialization.Formatters.Binary;

    using UnityEngine;

    public class TestBehaviour : MonoBehaviour
    {
        [SerializeField]
        private TestData properties;

        public void Awake()
        {
            if (this.properties == null)
            {
              //  this.properties = ScriptableObject.CreateInstance<TestData>();
//                this.properties = new TestData();
            }
        }

        public TestData Properties
        {
            get
            {
                 return this.properties; 
            }
        }
    }
}