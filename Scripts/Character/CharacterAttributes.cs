﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.PropertyModifiers.Scripts.Character
{
    using UnityEngine;

    /// <summary>
    /// Provides a <see cref="MonoBehaviour"/> that holds a characters properties.
    /// </summary>
    public class CharacterAttributes : MonoBehaviour
    {
        /// <summary>
        /// Defines the movement speed.
        /// </summary>
        [SerializeField]
        private CharacterProperties properties;

        /// <summary>
        /// Holds the reference to a property asset that contains character properties.
        /// </summary>
        [SerializeField]
        private string propertyAsset = string.Empty;

        /// <summary>
        /// Gets or sets the reference to a property asset that contains character properties.
        /// </summary>
        public string PropertyAsset
        {
            get
            {
                return this.propertyAsset;
            }

            set
            {
                this.propertyAsset = value;
            }
        }

        /// <summary>
        /// Gets the character properties.
        /// </summary>
        public CharacterProperties Properties
        {
            get
            {
                return this.properties;
            }
        }

        /// <summary>
        /// This function is called when the object becomes enabled and active.
        /// </summary>
        public void OnEnable()
        {
            if (this.properties == null)
            {
                this.properties = new CharacterProperties();
            }
        }

        /// <summary>
        /// Start is called just before any of the Update methods is called the first time.
        /// </summary>
        public void Start()
        {
            if (string.IsNullOrEmpty(this.propertyAsset))
            {
                return;
            }

            this.properties = ContentManager.ContentManager<string>.Instance.Load<CharacterProperties>(this.propertyAsset);
        }
    }
}
