/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.PropertyModifiers.Scripts.Character
{
    using System;

    using Codefarts.PropertyModifiers.Common;

    using UnityEngine;

    /// <summary>
    /// Provides a simple 2D axis movement controller.
    /// </summary>
    [RequireComponent(typeof(CharacterController)), RequireComponent(typeof(CharacterAttributes))]
    public class CharacterMovement2D : MonoBehaviour
    {
        /// <summary>
        /// Holds the value for the <see cref="Axis"/> property.
        /// </summary>
        [SerializeField]
        private CommonAxis axis;

        /// <summary>
        /// Holds a reference to a CharacterController reference.
        /// </summary>
        private CharacterController controller;

        /// <summary>
        /// Holds a reference to a <see cref="CharacterAttributes"/> type.
        /// </summary>
        private CharacterAttributes characterAttributes;

        /// <summary>
        /// Gets or sets the name for the speed property.
        /// </summary>
        public string SpeedPropertyName
        {
            get
            {
                return this.speedPropertyName;
            }

            set
            {
                this.speedPropertyName = value;
            }
        }

        /// <summary>
        /// Refers to the target transform that will be moved.
        /// </summary>
        [SerializeField]
        private Transform moveTarget;

        [SerializeField]
        private string speedPropertyName = "Speed";

        [NonSerialized]
        private FloatProperty movementSpeed;

        /// <summary>
        /// Gets or sets the axis that movement will occur along.
        /// </summary>
        public CommonAxis Axis
        {
            get
            {
                return this.axis;
            }

            set
            {
                this.axis = value;
            }
        }

        /// <summary>
        /// Start is called just before any of the Update methods is called the first time.
        /// </summary>
        public void Start()
        {
            this.characterAttributes = this.GetComponent<CharacterAttributes>();
        }

        /// <summary>
        /// Gets or sets the target transform that will be moved.
        /// </summary>
        public Transform MoveTarget
        {
            get
            {
                return this.moveTarget;
            }

            set
            {
                this.moveTarget = value;
            }
        }

        /// <summary>
        /// Start method called by unity.
        /// </summary>
        private void OnEnable()
        {
             // attempt to get the CharacterController reference
            this.controller = this.moveTarget.gameObject.GetComponent<CharacterController>();
        }

        /// <summary>
        /// Update method called by unity.
        /// </summary>
        private void Update()
        {
            if (this.movementSpeed == null && this.characterAttributes != null)
            {
                if (!this.characterAttributes.Properties.TryGetValue(this.speedPropertyName, out this.movementSpeed))
                {
                    return;
                }
            }

            // if no character controller just exit
            if (this.controller == null || this.movementSpeed == null)
            {
                return;
            }

            // calculate the move vector
            Vector3 moveDirection;
            switch (this.Axis)
            {
                case CommonAxis.XY:
                    moveDirection = new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"), 0);
                    break;

                case CommonAxis.XZ:
                    moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
                    break;

                case CommonAxis.ZY:
                    moveDirection = new Vector3(0, Input.GetAxis("Vertical"), Input.GetAxis("Horizontal"));
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }

            moveDirection = this.moveTarget.TransformDirection(moveDirection) * this.movementSpeed.Value;

            // move the controller
            this.controller.Move(moveDirection * Time.deltaTime);
        }
    }
}