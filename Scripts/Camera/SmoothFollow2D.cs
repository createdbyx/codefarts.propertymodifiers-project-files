/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/

namespace Codefarts.PropertyModifiers.Scripts.Camera
{
    using System;

    using Codefarts.PropertyModifiers.Common;

    using UnityEngine;

    /// <summary>
    /// Provides a MonoBehavior for one object to follow another object along a specified 2D axis.
    /// </summary>
    public class SmoothFollow2D : MonoBehaviour
    {
        /// <summary>
        /// Holds the value for the <see cref="Axis"/> property.
        /// </summary>
        [SerializeField]
        private CommonAxis axis;

        /// <summary>
        /// The distance from the target.
        /// </summary>
        [SerializeField]
        private float distance = 10;

        /// <summary>
        /// The lag time it takes for the follower to come to move.
        /// </summary>
        [SerializeField]
        private float smoothTime = 0.3f;

        /// <summary>
        /// The target to be followed.
        /// </summary>
        [SerializeField]
        private Transform target;

        /// <summary>
        /// Holds a reference to the transform.
        /// </summary>
        private Transform transformReference;

        /// <summary>
        /// Stores the movement velocity.
        /// </summary>
        private Vector2 velocity;

        /// <summary>
        /// Gets or sets the axis that following will occur on.
        /// </summary>
        public CommonAxis Axis
        {
            get
            {
                return this.axis;
            }

            set
            {
                this.axis = value;
            }
        }

        /// <summary>
        /// Gets or sets the distance from the target.
        /// </summary>
        public float Distance
        {
            get
            {
                return this.distance;
            }

            set
            {
                this.distance = value;
            }
        }

        /// <summary>
        /// Gets or sets the lag time it takes for the follower to come to move.
        /// </summary>
        public float SmoothTime
        {
            get
            {
                return this.smoothTime;
            }

            set
            {
                this.smoothTime = value;
            }
        }

        /// <summary>
        /// Gets or sets the target to be followed.
        /// </summary>
        public Transform Target
        {
            get
            {
                return this.target;
            }

            set
            {
                this.target = value;
            }
        }

        /// <summary>
        /// Called on game start.
        /// </summary>
        private void Start()
        {
            this.transformReference = this.transform;
        }

        /// <summary>
        /// Updates the following targets position.
        /// </summary>
        private void Update()
        {
            // determine what axis to follow on
            switch (this.Axis)
            {
                case CommonAxis.XY:
                    this.transformReference.position = new Vector3(
                               Mathf.SmoothDamp(this.transformReference.position.x, this.target.position.x, ref this.velocity.x, this.smoothTime),
                               Mathf.SmoothDamp(this.transformReference.position.y, this.target.position.y, ref this.velocity.y, this.smoothTime),
                               this.target.position.z - this.distance);
                    break;
             
                case CommonAxis.XZ:
                    this.transformReference.position = new Vector3(
                            Mathf.SmoothDamp(this.transformReference.position.x, this.target.position.x, ref this.velocity.x, this.smoothTime),
                            this.target.position.y - this.distance,
                            Mathf.SmoothDamp(this.transformReference.position.z, this.target.position.z, ref this.velocity.y, this.smoothTime));
                    break;
             
                case CommonAxis.ZY:
                    this.transformReference.position = new Vector3(
                              Mathf.SmoothDamp(this.transformReference.position.z, this.target.position.z, ref this.velocity.x, this.smoothTime),
                              Mathf.SmoothDamp(this.transformReference.position.y, this.target.position.y, ref this.velocity.y, this.smoothTime),
                              this.target.position.x - this.distance);
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}