﻿/*
<copyright>
  Copyright (c) 2012 Codefarts
  All rights reserved.
  contact@codefarts.com
  http://www.codefarts.com
</copyright>
*/
namespace Codefarts.PropertyModifiers.Scripts
{
    using Codefarts.PropertyModifiers.Unity;

    using UnityEngine;

    /// <summary>
    /// Sets up the scene, and registers readers etc.
    /// </summary>
    public class SceneSetup : MonoBehaviour
    {
        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        public void Awake()
        {
            var content = ContentManager.ContentManager<string>.Instance;
            content.Register(new PropertiesReader());
        }
    }
}
