﻿Copyright (c) 2013 Codefarts 
contact@codefarts.com
http://www.codefarts.com/
All rights reserved.

Property Modifiers allow you to attach and remove modifiers to named properties in order to adjust there value.	
Modifiers are also stackable. Think of property modifiers like the buff and debuffs you character can have in popular RPG games
like world of warcraft etc.

Documentation is available in the documentation folder "Documentation\PropertyModifiersDocumentation.zip". 